#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


#define LMAX 1024
#define NMAX 512
#define AMAX 1024


static int LSIZE = 0;
static char line[LMAX];


typedef struct entry {
    char name[NMAX];
    char street_address[AMAX];
    char city[50];
    char state[3];
    char zip[20];
    char country[20];
    char phone_number[15];
} Entry;


typedef struct book {
    int capacity;
    int size;
    Entry ** entries;
} Book;


int get_cmd(char *input)
{
    /*cmd list:
     * exit */
    char *ptr = strstr(input, "exit");
    if (ptr != NULL) {
        printf("Exiting\n");
        return 0;
    }
    ptr = strstr(input, "create");
    if (ptr != NULL) {
        printf("Creating new address book.\n");
        return 1;
    }
    return 10;
}


Book * create_book()
{
    Book *book = malloc(sizeof(Book));
    book->size = 0;
    book->capacity = 0;
    book->entries = NULL;
    return book;
}


static void add_entry(void)
{
}


static void remove_entry(void)
{
}


static void find_entry(void)
{
}


static void invalid_cmd(void)
{
}


static void handle_request(char line[])
{
    if (!(strncmp(line, "add", 4))) {
        add_entry();
    } else if (!strncmp(line, "remove", 6)) {
        remove_entry();
    } else if (!strncmp(line, "find", 4)) {
        find_entry();
    } else {
        invalid_cmd();
    }
    return;
}


static void display_prompt(void)
{
    printf("(addr) $> ");
    fflush(stdout);
}


static ssize_t Read(int filedes, void * buffer, size_t size)
{
    ssize_t amt = read(filedes, buffer, size);
    if (amt < 0) {
        perror("read");
        exit(1);
    }
    return amt;
}


int main()
{

    display_prompt();
    while((LSIZE = Read(STDIN_FILENO, line, LMAX-1)) > 0) {
        if (!strncmp(line, "exit", 4))
            break;
        handle_request(line);
        display_prompt();
    }

    printf("exiting\n");
    return EXIT_SUCCESS;
}
