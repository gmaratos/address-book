OPTS=-fno-pie -no-pie -fno-builtin -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -Werror -std=c17 -Wpedantic -O0 -g


all: book


book: book.o
	gcc $(OPTS) -o book book.o


book.o: book.c
	gcc $(OPTS) -c book.c

clean:
	rm -f *.o book
