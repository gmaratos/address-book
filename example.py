# import preamble

import json
from getpass import getpass
import sys
import os.path as osp

from cryptography.fernet import Fernet
import pandas as pd

# important global variables

user = 'c3lnp9k57'
root_path = f'/home/{user}/Desktop/files/z_Files'

# address validator https://www.ups.com/address_validator/search?loc=en_US
## overall structure
### encrypted binary -> jsonl -> DataFrame.from_records -> jsonl -> encrypted binary

# jsonl interface

class jsonl:

    def dumps(data):
        """data is a list of objects"""

        return ''.join([json.dumps(entry) + '\n' for entry in data])

    def loads(raw_data):

        return [json.loads(entry) for entry in raw_data.strip().split('\n')]

# loading and unloading

def load_data(path, key):
    """loads and the address book (encrypted file)"""

    with open(path, 'rb') as f:
        data = f.read()
    data = Fernet(key).decrypt(data)
    data = data.decode('utf-8')
    data = jsonl.loads(data)
    df = pd.DataFrame.from_records(data)
    return df

def save_df(path, key, df):
    """save the dataframe back into an encrypted file"""

    data = df.to_json(orient='records', lines=True)
    data = jsonl.dumps(data)
    data = data.encode('utf-8')
    data = Fernet(key).encrypt(data)
    with open(path, 'wb') as f:
        f.write(data)

def create(book):

    ## create key for user
    key = Fernet.generate_key()
    addr_path = osp.join(root_path, 'book.addr')
    ## save book in encrypted format
    if book == None: ### user has an empty book
        print('There are no entries in the book.')
        return

# shell prompt

book = None
while True:
    ## prompt and read inp
    print('(addr) $> ', end='')
    user_input = input().split()
    ## initial checks
    if (length := len(user_input)) < 1:
        continue
    if length == 1 and user_input[0] == 'exit':
        print('Exiting')
        sys.exit(0)
    ## parse commands
    if user_input[0] == 'create':
        print('Creating new address book')
        create(book)
    elif user_input[0] == 'load':
        print('Loading address book')

print('Would you like to create an address [y,n]? ')
answer = input()

# get key

print(f'Address book is at: {path}')
key = getpass('Enter Password to unlock: ')
key = key.encode('utf-8')
df = load_data(path, key)

import pdb;pdb.set_trace()

## load data

## f = Fernet(key) key is bytes
## f.encrypt(bytes) -> bytes
## f.decrypt(bytes) -> string

# pandas

import pdb;pdb.set_trace()
